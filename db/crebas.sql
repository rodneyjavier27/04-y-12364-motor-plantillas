CREATE TABLE IF NOT EXISTS `Integrantes` (
    `ID_Integrantes` INT UNIQUE,
    `Nombre` VARCHAR(255) NOT NULL,
    `Apellido` VARCHAR(255) NOT NULL,
    `Codigo` INT NOT NULL,
    `Matricula` VARCHAR(255) NOT NULL,
    `deleted` BOOLEAN,
    PRIMARY KEY(`ID_Integrantes`)
);

-- Definición de la tabla Tipo_de_Media
CREATE TABLE IF NOT EXISTS `Tipo_de_Media` (
    `ID_TipoMedio` INTEGER PRIMARY KEY, -- Se genera automáticamente
    `NombreTipo` VARCHAR(255),
    `deleted` BOOLEAN,
    `order_num` INT
);

CREATE TABLE IF NOT EXISTS `Media` (
    `ID_Media` INTEGER PRIMARY KEY, 
    `ID_Integrante` INT,
    `ID_TipoMedio` INT,
    `URL_Media` VARCHAR(255),
    `deleted` BOOLEAN,
    `order_num` INT,
    FOREIGN KEY(`ID_Integrante`) REFERENCES `Integrantes`(`ID_Integrantes`) ON UPDATE NO ACTION ON DELETE NO ACTION,
    FOREIGN KEY(`ID_TipoMedio`) REFERENCES `Tipo_de_Media`(`ID_TipoMedio`) ON UPDATE NO ACTION ON DELETE NO ACTION
);


