// Datos de los integrantes
exports.integrantes = [
    {nombre: "Lujan", apellido: "Caceres", imagen: "", Codigo: "01", matricula: "Y18624"}, //indice 0
    {nombre: "Christian", apellido: "Salinas", imagen: "", Codigo: "02", matricula: "Y25366"}, //indice 1
    {nombre: "Steven", apellido: "Lopez", imagen: "", Codigo: "03", matricula: "Y12887"}, //indice 2
    {nombre: "Rodney", apellido: "Ramirez", imagen: "", Codigo: "04", matricula: "Y12363"}, //indice 3
];

exports.media = {
    //Lujan
    "Y18624": {
        youtube: "https://www.youtube.com/embed/Ci5raxp37QE",  
        imagen: "../../images/isla.jpeg", 
        dibujo: "../../images/paint_lujan.png", 
        matricula: "Y18624",
        colores: {
            fondo: "#FFFFFF",
            texto: "#333",
            headerBackground: "#491D57",
            navBackground: "#491D57",
            navText: "#FFFFFF",
            sectionBackground: "#ca98da"
        }
    },

    "Y25366": {
        youtube: "https://www.youtube.com/embed/ZDs_f_ZdluU", 
        imagen: "../../images/imgrepresentativa.jpg", 
        dibujo: "../../images/imgpropia.png", 
        matricula: "Y25366",
        colores: {
            fondo: "#FFFF00", // Amarillo
            texto: "#333",
            headerBackground: "black",
            navBackground: "#FFD700",
            navText: "#FFFFFF",
            sectionBackground: "black"
        }
    },

    "Y12887": {
        youtube: "https://www.youtube.com/embed/0aQPX_Iqu-A?si=yrpnMk0IUVFkQtif", 
        imagen: "../../images/mejores-anime-largos.webp", 
        dibujo: "../../images/20240410_192335.jpg", 
        matricula: "Y12887",
        colores: {
            fondo: "#a298da", // Azul
            texto: "#FFFFFF",
            headerBackground: "#1d2857",
            navBackground: "#261d57",
            navText: "#FFFFFF",
            sectionBackground: "#1d2857"
        }
    },

    "Y12363": {
        youtube: "https://www.youtube.com/embed/QcaZLO59Vpc?si=qS5-wBEcKhgr_DaM", 
        imagen: "../../images/mathi.jpg", 
        dibujo: "../../images/imagen.png", 
        matricula: "3850665",
        colores: {
            fondo: "#2E8B57", // Rojo
            texto: "#FFFFFF",
            headerBackground: "#32CD32",
            navBackground: "#008000",
            navText: "#FFFFFF",
            sectionBackground: "#008000"
        }
    }
};


//exports.integrantes = integrantes;
//exports.media2 = media;
